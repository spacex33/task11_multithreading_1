package wait_notify_game;

public class PingPongGame {

    public void start() {
        run("ping");
        run("pong");
    }

    private void run(String text) {
        new Thread(() -> {
            synchronized (this) {

                for (int i = 0; i < 100; i++) {
                    System.out.println(text);

                    try {
                        notify();
                        Thread.sleep(300);
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                notify();
            }
        }).start();
    }
}
