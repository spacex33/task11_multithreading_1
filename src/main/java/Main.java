import fibonacci_numbers.FibonacciTask;

import java.util.concurrent.ExecutionException;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        new FibonacciTask(5).executeInSingleThread();

        System.out.println(new FibonacciTask(5).getSumOfValuesInThreadPool(2));
    }
}
