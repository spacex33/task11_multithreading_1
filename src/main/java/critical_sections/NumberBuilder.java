package critical_sections;

public class NumberBuilder {

    private int number;
    private final Object object = new Object();
    private final Object object2 = new Object();
    private final Object object3 = new Object();

    public NumberBuilder(int number) {
        this.number = number;
        System.out.println(this + " initial");
    }

    public void add(int number) {
//        synchronized (this) {
        synchronized (object) {
            System.out.println(this
                    + " "
                    + number
                    + " added, result: "
                    + (this.number += number)
                    + " by #"
                    + Thread.currentThread().getId());
        }
    }

    public void subtract(int number) {
//        synchronized (this) {
        synchronized (object2) {
            System.out.println(this
                    + " "
                    + number
                    + " subtracted, result: "
                    + (this.number -= number)
                    + " by #"
                    + Thread.currentThread().getId());
        }
    }

    public void multiply(int number) {
//        synchronized (this) {
        synchronized (object3) {
            System.out.println(this
                    + " "
                    + number
                    + " multiplied, result: "
                    + (this.number *= number)
                    + " by #"
                    + Thread.currentThread().getId());
        }
    }

    @Override
    public String toString() {
        return "NumberBuilder{" +
                "number=" + number +
                '}';
    }
}
