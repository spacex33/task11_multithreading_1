package fibonacci_numbers;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibonacciTask {

    private int number;
    private FibonacciSequence sequence = new FibonacciSequence();
    private ExecutorService executor;

    public FibonacciTask(int number) {
        this.number = number;
    }

    public void executeBySimpleThread() {
        new Thread(() -> {
            synchronized (FibonacciTask.class) {
                printFibonacciSequence(number, 200);
            }
        }).start();
    }

    public void executeInSingleThread() {
        executor = Executors.newSingleThreadExecutor();
        executor.execute(this::executeBySimpleThread);
        executor.shutdown();
    }

    public void executeInCachedThreadPool() {
        executor = Executors.newCachedThreadPool();
        executor.execute(this::executeBySimpleThread);
        executor.shutdown();
    }

    public void executeInFixedThreadPool(int poolAmount) {
        executor = Executors.newFixedThreadPool(poolAmount);
        executor.execute(this::executeBySimpleThread);
        executor.shutdown();
    }

    public int getSumOfValuesInThreadPool(int poolAmount) {
        int result = 0;
        executor = Executors.newFixedThreadPool(poolAmount);
        Future<Integer> future = executor.submit(() -> sequence.sum(number));
        try {
            result = future.get();
            executor.shutdown();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }

    private void printFibonacciSequence(int number, int millis) {
        for (int i = 0; i < number; i++) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print(sequence.produce(i) + " ");
        }
    }



}
