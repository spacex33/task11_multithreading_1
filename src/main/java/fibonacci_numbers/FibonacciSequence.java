package fibonacci_numbers;

public class FibonacciSequence {

    public int produce(int number) {
        if (number <= 1)
            return number;
        return produce(number - 1) + produce(number - 2);
    }

    public int sum(int number) {
        int sum = 0;

        for (int i = 0; i < number; i++) {
            sum += produce(i);
        }

        return sum;
    }
}
