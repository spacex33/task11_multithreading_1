package pipe_task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        PipeSender pipeSender = new PipeSender("Hello World!");
        PipeReceiver pipeReceiver = new PipeReceiver(pipeSender);
        ExecutorService executorService = Executors.newCachedThreadPool();

        try {
            executorService.execute(pipeReceiver);
            executorService.execute(pipeSender);
        } finally {
            executorService.shutdown();
        }

    }
}
