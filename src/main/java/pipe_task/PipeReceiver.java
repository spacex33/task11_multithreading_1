package pipe_task;

import java.io.IOException;
import java.io.PipedReader;

public class PipeReceiver implements Runnable{

    private PipedReader pipedReader;
    public PipeReceiver(PipeSender pipeSender) {
        try {
            pipedReader = new PipedReader(pipeSender.getPipedWriter());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (true) {
                System.out.print((char)pipedReader.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
