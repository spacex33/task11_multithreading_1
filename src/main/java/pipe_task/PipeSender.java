package pipe_task;

import java.io.IOException;
import java.io.PipedWriter;

public class PipeSender implements Runnable{

    private String message;
    private PipedWriter pipedWriter = new PipedWriter();

    public PipeSender(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        try {
            for (char character : message.toCharArray()) {
                pipedWriter.write(character);
                Thread.sleep(300);
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public PipedWriter getPipedWriter() {
        return pipedWriter;
    }
}
