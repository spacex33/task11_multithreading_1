package sleep_task;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        int taskQuantity = Integer.parseInt(args[0]);
        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(taskQuantity);
        for (int i = 0; i < taskQuantity; i++) {
            int randomDelay = new Random().nextInt(10);
            System.out.println(randomDelay);
            threadPool.schedule(
                    new Thread(() -> System.out.println(Thread.currentThread() + " delay: " + randomDelay)),
                    randomDelay,
                    TimeUnit.SECONDS
            );
        }
        threadPool.shutdown();
    }
}
